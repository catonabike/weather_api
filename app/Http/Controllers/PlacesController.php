<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class PlacesController extends Controller
{
    /**
     * Display a listing of British cities and OWM codes.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cityList = City::where( 'countryCode', '=', 'GB' )->get();

        $response = [
            'code' => 200,
            'message' => 'OWM list of UK cities with codes',
            'cities' => $cityList,
        ];

        return response()->json( $response );
    }

    /**
     * Display the city for the given reference code.
     *
     * @param  int  $id  -->(OWM id code)
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $city = City::where( 'cityRef', '=', $id )->get();

        if( $city->isEmpty() )
        {
            $response = [
                'code' => 404,
                'message' => "No city could be found relating to OWN code id: $id",
                'city' => $city,
            ];
        }
        else
        {
            $response = [
                'code' => 200,
                'message' => "City for OWN code id: $id",
                'city' => $city,
            ];
        }

        return response()->json( $response );
    }

    /**
     * Display the cities the contain the string search term.
     *
     * @param  string  $term
     * @return \Illuminate\Http\Response
     */
    public function search($term)
    {
        $cities = City::where([
                [ 'cityName', 'LIKE', '%'.$term.'%' ],
                [ 'countryCode', '=', 'GB' ]
            ])->get();

        if( $cities->isEmpty() )
        {
            $response = [
                'code' => 404,
                'message' => "No cities could be found matching the term: $term",
                'city' => $cities,
            ];
        }
        else
        {
            $response = [
                'code' => 200,
                'message' => "These are the city names containing the search term: $term",
                'city' => $cities,
            ];
        }

        return response()->json( $response );
    }
}
